/* eslint-disable mocha/no-setup-in-describe */
const assert = require('assert');
const Calculator = require('../src/Calculator');

describe('Test Calculator', function () {
  before('before testing', function () {
  });

  describe('subtract()', function () {
    const calculator = new Calculator();
    calculator.calculatorResult = 2;
    calculator.subtract(2);

    const result = calculator.getResult();
    it('subtract', function () {
      assert.strictEqual(result, 0);
    });
  });
  describe('multiply()', function () {
    const calculator = new Calculator();
    calculator.calculatorResult = 2;
    calculator.multiply(2);

    const result = calculator.getResult();
    it('multiply', function () {
      assert.strictEqual(result, 4);
    });
  });
  describe('add()', function () {
    const calculator = new Calculator();
    calculator.calculatorResult = 2;
    calculator.add(2);

    const result = calculator.getResult();
    it('add', function () {
      assert.strictEqual(result, 4);
    });
  });
  describe('divide()', function () {
    const calculator = new Calculator();
    calculator.calculatorResult = 2;
    calculator.divide(2);

    const result = calculator.getResult();

    it('divide', function () {
      assert.strictEqual(result, 1);
    });
  });
});
